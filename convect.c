static char help[] = "Linear advection using first order FVM\n";
#include <petsc.h>

PETSC_STATIC_INLINE
void Waxpy(PetscInt dim, PetscScalar a, const PetscScalar *x, const PetscScalar *y, PetscScalar *w)
{
   PetscInt d; for (d = 0; d < dim; ++d) w[d] = a*x[d] + y[d];
}

PETSC_STATIC_INLINE
PetscScalar Dot(PetscInt dim, const PetscScalar *x, const PetscScalar *y)
{
   PetscScalar sum = 0.0; PetscInt d; for (d = 0; d < dim; ++d) sum += x[d]*y[d]; return sum;
}

PETSC_STATIC_INLINE
PetscReal Norm(PetscInt dim, const PetscScalar *x)
{
   return PetscSqrtReal(PetscAbsScalar(Dot(dim,x,x)));
}

// advection speed
void advection_speed(const PetscReal *X, PetscReal *speed)
{
   PetscReal x = X[0];
   PetscReal y = X[1];
   speed[0] = -y;
   speed[1] =  x;
}

// initial condition
PetscReal initial_condition(const PetscReal *X)
{
   PetscReal x = X[0];
   PetscReal y = X[1];
   PetscReal r2 = PetscPowReal(x-0.5,2) + PetscPowReal(y,2);
   return PetscExpReal(-50.0*r2);
}

// upwind numerical flux
PetscReal numerical_flux(const PetscReal a, const PetscReal ul, const PetscReal ur)
{
   return (a > 0.0) ? a*(ul) : a*(ur);
}

// set initial condition for the given dm and vector
PetscErrorCode SetIC(DM dm, Vec U)
{
   PetscErrorCode ierr;
   PetscScalar *u;

   PetscFunctionBegin;
   PetscInt c, cStart, cEnd; // cells
   PetscReal area, centroid[3], normal[3]; // geometric data
   // get cell stratum owned by processor
   ierr = DMPlexGetHeightStratum(dm, 0, &cStart, &cEnd); CHKERRQ(ierr);
   // get array for U
   ierr = VecGetArray(U, &u);
   // loop over cells and assign values
   for(c=cStart; c<cEnd; ++c)
   {
      PetscInt label;
      ierr = DMGetLabelValue(dm, "vtk", c, &label); CHKERRQ(ierr);
      // write into Global vector if the cell is a real cell
      if(label == 1)
      {
         PetscReal X[2]; // cell centroid
         ierr = DMPlexComputeCellGeometryFVM(dm, c, &area, centroid, normal); CHKERRQ(ierr);
         X[0] = centroid[0]; X[1] = centroid[1];
         u[c] = initial_condition(X);
      }
   }
   ierr = VecRestoreArray(U, &u);
   PetscFunctionReturn(0);
}

static PetscErrorCode OutputVTK(DM dm, const char *filename, PetscViewer *viewer)
{
   PetscFunctionBegin;
   PetscErrorCode ierr;

   ierr = PetscViewerCreate(PetscObjectComm((PetscObject)dm), viewer);CHKERRQ(ierr);
   ierr = PetscViewerSetType(*viewer, PETSCVIEWERVTK);CHKERRQ(ierr);
   ierr = PetscViewerPushFormat(*viewer, PETSC_VIEWER_VTK_VTU); CHKERRQ(ierr);
   ierr = PetscViewerFileSetName(*viewer, filename);CHKERRQ(ierr);
   PetscFunctionReturn(0);
}

// compute residual and store it to R vector
PetscErrorCode ComputeResidual(DM dm, Vec U, Vec R)
{
   PetscErrorCode ierr;

   PetscFunctionBegin;
   Vec Ul; // local vector
   // get local vector from dm
   ierr = DMGetLocalVector(dm, &Ul); CHKERRQ(ierr);
   // initialize R
   ierr = VecSet(R, 0.0); CHKERRQ(ierr);

   // copy values from Global to Local vector (including ghost values)
   ierr = DMGlobalToLocalBegin(dm, U, INSERT_VALUES, Ul); CHKERRQ(ierr);
   ierr = DMGlobalToLocalEnd(dm, U, INSERT_VALUES, Ul); CHKERRQ(ierr);

   // get arrays
   PetscReal *Rarr, *Uarr;
   ierr = VecGetArray(Ul, &Uarr); CHKERRQ(ierr);
   ierr = VecGetArray(R, &Rarr); CHKERRQ(ierr);

   // get geometric data for faces and cells
   DM dmFace, dmCell, dummy; // DMs for vectors with face and cell data
   PetscFV fvm;
   Vec cellGeom, faceGeom;
   const PetscScalar *fgeom, *cgeom; // arrays corresponding to vectors
   PetscFVFaceGeom *fg; // struct with face geometry information
   PetscFVCellGeom *cg; // struct with cell geometry information
   ierr = PetscFVCreate(PETSC_COMM_WORLD, &fvm); CHKERRQ(ierr); // FV object
   ierr = PetscFVSetType(fvm, PETSCFVUPWIND); CHKERRQ(ierr);
   ierr = DMPlexGetDataFVM(dm, fvm, &cellGeom, &faceGeom, &dummy); CHKERRQ(ierr);
   ierr = VecGetDM(faceGeom, &dmFace); CHKERRQ(ierr);
   ierr = VecGetDM(cellGeom, &dmCell); CHKERRQ(ierr);
   ierr = VecGetArrayRead(faceGeom, &fgeom); CHKERRQ(ierr);
   ierr = VecGetArrayRead(cellGeom, &cgeom); CHKERRQ(ierr);

   PetscInt  f, fStart, fEnd; // faces
   PetscInt  ss;
   const PetscInt *supp; // support (neighbouring cells)
   PetscReal ul, ur, flux, speed[2], norm_speed;
   PetscReal len, area, Xf[2], Nf[2]; // geometric data

   // get faces and loop over them
   ierr = DMPlexGetHeightStratum(dm, 1, &fStart, &fEnd); CHKERRQ(ierr);
   for(f=fStart; f<fEnd; ++f)
   {
      // get support
      ierr = DMPlexGetSupportSize(dm, f, &ss); CHKERRQ(ierr);
      ierr = DMPlexGetSupport(dm, f, &supp); CHKERRQ(ierr);

      // get face geometry
      ierr = DMPlexPointLocalRead(dmFace, f, fgeom, &fg); CHKERRQ(ierr);
      Xf[0] = fg->centroid[0]; Xf[1] = fg->centroid[1]; // face centroid
      len = Norm(2, fg->normal); // normals are area scaled
      Nf[0] = fg->normal[0]/len;   Nf[1] = fg->normal[1]/len; // face normal
      // get advection speed
      advection_speed(Xf, speed);
      norm_speed = Dot(2, speed, Nf); // speed normal to the face

      if(ss == 1) // boundary face
      {
         ul = Uarr[supp[0]];
         ur = 0; // Dirichlet BC
         flux = numerical_flux(norm_speed, ul, ur);

         PetscInt label;
         ierr = DMGetLabelValue(dm, "vtk", supp[0], &label); CHKERRQ(ierr);
         // if not a ghost cell compute residual
         if(label == 1)
         {
            // get cell geometry
            ierr = DMPlexPointLocalRead(dmCell, supp[0], cgeom, &cg); CHKERRQ(ierr);
            area = cg->volume; 
            Rarr[supp[0]] -= flux * len / area;
         }
      }
      else // interior face
      {
         ul = Uarr[supp[0]];
         ur = Uarr[supp[1]];
         flux = numerical_flux(norm_speed, ul, ur);
         PetscInt label;
         ierr = DMGetLabelValue(dm, "vtk", supp[0], &label); CHKERRQ(ierr);
         // if not a ghost cell compute residual
         if(label == 1)
         { 
            // get cell geometry
            ierr = DMPlexPointLocalRead(dmCell, supp[0], cgeom, &cg); CHKERRQ(ierr);
            area = cg->volume;
            Rarr[supp[0]] -= flux * len / area;
         }
         ierr = DMGetLabelValue(dm, "vtk", supp[1], &label); CHKERRQ(ierr);
         if(label == 1)
         {
            // get cell geometry
            ierr = DMPlexPointLocalRead(dmCell, supp[1], cgeom, &cg); CHKERRQ(ierr);
            area = cg->volume;
            // add contribution
            Rarr[supp[1]] += flux * len / area;
         }
      }  
   }

   ierr = VecRestoreArray(Ul, &Uarr); CHKERRQ(ierr);
   ierr = VecRestoreArray(R, &Rarr); CHKERRQ(ierr);
   // cleanup
   ierr = PetscFVDestroy(&fvm); CHKERRQ(ierr);
   ierr = VecRestoreArrayRead(faceGeom, &fgeom); CHKERRQ(ierr);
   ierr = VecRestoreArrayRead(cellGeom, &cgeom); CHKERRQ(ierr);
   ierr = DMRestoreLocalVector(dm, &Ul); CHKERRQ(ierr);
   PetscFunctionReturn(0);
}

// update solution using forward euler
PetscErrorCode UpdateSolution(DM dm, Vec U, Vec R, PetscReal dt)
{
   PetscErrorCode ierr;
   
   PetscFunctionBegin;
   ierr = ComputeResidual(dm, U, R); CHKERRQ(ierr);
   // U = U + dt*R
   ierr = VecAXPY(U, dt, R); CHKERRQ(ierr); 
   PetscFunctionReturn(0);
}

int main(int argc, char **argv)
{
   PetscErrorCode ierr;
   PetscViewer    viewer;
   DM             dm, dmDist = NULL;
   PetscSection   sec;
   PetscInt       dim = 2, numFields = 1, overlap = 1, numBC, i;
   PetscInt       numComp[1]; // number of components per field
   PetscInt       numDof[3];
   PetscInt       bcField[1]; // number of BCs for each field
   IS             bcPointsIS[1]; // contains boundary faces
   PetscBool      interpolate = PETSC_TRUE, status;
   Vec            U, R; // solution and residual
   PetscMPIInt    rank, size;
   PetscReal      lower[3], upper[3]; // lower left and upper right coordinates of domain
   PetscInt       cells[2]; 
   char           filename[PETSC_MAX_PATH_LEN];

   // define domain properties
   cells[0] = 100; cells[1] = 100;
   lower[0] = -1; lower[1] = -1; lower[2] = 0;
   upper[0] =  1; upper[1] =  1; upper[2] = 0;

   ierr = PetscInitialize(&argc, &argv, (char*)0, help); CHKERRQ(ierr);
   MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
   MPI_Comm_size(PETSC_COMM_WORLD, &size);
   
   // get number of cells in x and y direction
   ierr = PetscOptionsGetInt(NULL, NULL, "-nx", &cells[0], &status); CHKERRQ(ierr);
   ierr = PetscOptionsGetInt(NULL, NULL, "-ny", &cells[1], &status); CHKERRQ(ierr);
   
   // set time parameters
   PetscReal t = 0.0, tf = 2.0*M_PI, dt = 0.001;
   ierr = PetscOptionsGetReal(NULL, NULL, "-tf", &tf, &status); CHKERRQ(ierr);

   // get mesh filename
   ierr = PetscOptionsGetString(NULL, NULL, "-mesh", filename, PETSC_MAX_PATH_LEN, &status);
   if(status) // gmsh file provided by user
   {
      char file[PETSC_MAX_PATH_LEN];
      ierr = PetscStrcpy(file, filename); CHKERRQ(ierr);
      ierr = PetscSNPrintf(filename, sizeof filename,"./%s", file); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD, "Reading gmsh %s ... ", file); CHKERRQ(ierr);
      ierr = DMPlexCreateGmshFromFile(PETSC_COMM_WORLD, filename, interpolate, &dm); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD, "Done\n"); CHKERRQ(ierr);
   }
   else
   {
      // create the mesh
      ierr = PetscPrintf(PETSC_COMM_WORLD, "Generating mesh ... "); CHKERRQ(ierr);
      ierr = DMPlexCreateBoxMesh(PETSC_COMM_WORLD, dim, PETSC_TRUE,
                                 cells, lower, upper, NULL, interpolate, &dm); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD, "Done\n"); CHKERRQ(ierr);
   }
   // distribute mesh over processes;
   ierr = DMPlexDistribute(dm, overlap, NULL, &dmDist); CHKERRQ(ierr);
   if(dmDist) 
   {
      ierr = DMDestroy(&dm); CHKERRQ(ierr);
      dm = dmDist;
   }
   // print mesh information
   ierr = PetscPrintf(PETSC_COMM_WORLD, "overlap: %d, " 
                                        "distributed among %d processors\n",
                                        overlap, size); CHKERRQ(ierr);
   // construct ghost cells
   PetscInt nGhost; // number of ghost cells
   DM dmG; // DM with ghost cells
   ierr = DMPlexConstructGhostCells(dm, NULL, &nGhost, &dmG); CHKERRQ(ierr);
   if(dmG) 
   {
      ierr = DMDestroy(&dm); CHKERRQ(ierr);
      dm = dmG;
   } 

   // create scalar field for solution u
   numComp[0] = 1;
   for(i=0; i<numFields*(dim+1); ++i) numDof[i] = 0;
   // define u at cell centers
   numDof[0*(dim+1)+dim] = 1;

   // set BC
   numBC = 1;
   bcField[0] = 0; // Dirichlet BC on u at boundary
   if(status) // gmsh file given
   {
      ierr = DMGetStratumIS(dm, "Face Sets", 1, &bcPointsIS[0]); CHKERRQ(ierr);
   }
   else // mesh created by DMPlex (gives "marker" label to boundary faces)
   {
      ierr = DMGetStratumIS(dm, "marker", 1, &bcPointsIS[0]); CHKERRQ(ierr);
   }

   // create a PetscSection with this layout
   ierr = DMSetNumFields(dm, numFields); CHKERRQ(ierr);
   ierr = DMPlexCreateSection(dm, NULL, numComp, numDof, numBC,
                              bcField, NULL, bcPointsIS, NULL, &sec); CHKERRQ(ierr);
   ierr = ISDestroy(&bcPointsIS[0]); CHKERRQ(ierr);
   ierr = PetscSectionSetFieldName(sec, 0, "u"); CHKERRQ(ierr);
   // set the section for dm
   ierr = DMSetLocalSection(dm, sec); CHKERRQ(ierr);
   ierr = DMSetAdjacency(dm, 0, PETSC_TRUE, PETSC_FALSE); CHKERRQ(ierr);

   ierr = DMSetUp(dm); CHKERRQ(ierr);
   ierr = DMView(dm, PETSC_VIEWER_STDOUT_WORLD);

   // get global vector for solution
   ierr = DMCreateGlobalVector(dm, &U); CHKERRQ(ierr);
   ierr = PetscObjectSetName((PetscObject) U, "u"); CHKERRQ(ierr);
   ierr = SetIC(dm, U); CHKERRQ(ierr);

   // write initial solution to vtk
   ierr = OutputVTK(dm, "sol-000.vtu", &viewer); CHKERRQ(ierr);
   ierr = VecView(U, viewer); CHKERRQ(ierr);
   ierr = PetscViewerDestroy(&viewer); CHKERRQ(ierr);
   ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrote sol-000.vtu\n"); CHKERRQ(ierr);
   
   ierr = VecDuplicate(U, &R); CHKERRQ(ierr);

   PetscInt counter = 1, vtkInterval = 100, iter = 0; // saving frequency of solution 
   while(t < tf)
   {  
      if(t + dt > tf)
         dt = tf - t;
      ierr = UpdateSolution(dm, U, R, dt); CHKERRQ(ierr);
      t += dt; ++iter;
      ierr = PetscPrintf(PETSC_COMM_WORLD, "iter: %d, t = %f\n", iter, t); CHKERRQ(ierr);
      if(iter % vtkInterval == 0 || t == tf)
      {
         ierr = PetscSNPrintf(filename, sizeof(filename),
                              "sol-%03D.vtu", counter); CHKERRQ(ierr);
         ierr = OutputVTK(dm, filename, &viewer); CHKERRQ(ierr);
         ierr = VecView(U, viewer); CHKERRQ(ierr);
         ierr = PetscViewerDestroy(&viewer); CHKERRQ(ierr);
         ierr = PetscPrintf(PETSC_COMM_WORLD, "Wrote %s\n", filename); CHKERRQ(ierr);
         ++counter;
      }
   }

   // cleanup
   ierr = VecDestroy(&R); CHKERRQ(ierr);
   ierr = DMRestoreGlobalVector(dm, &U); CHKERRQ(ierr);
   ierr = PetscSectionDestroy(&sec); CHKERRQ(ierr);
   ierr = DMDestroy(&dm); CHKERRQ(ierr);
   ierr = PetscFinalize(); CHKERRQ(ierr);
   return ierr;
}
