# README #

Compile:

	make convect

Run:

	// Read mesh from a Gmsh file
	mpiexec -n <numproc> ./convect -tf <FinalTime> -mesh <GmshFile>
	or
	// Generate own mesh
	mpiexec -n <numproc> ./convect -tf <FinalTime> -nx <NumCellsX> -ny <NumCellsY>